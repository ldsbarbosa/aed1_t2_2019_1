#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "arvore.h"


struct Stack * create(int size) { 
    struct Stack * stack = (struct Stack *) malloc(sizeof(struct Stack)); 
    stack->size = size; 
    stack->top = -1; 
    stack->array = (struct nodo **) malloc(stack->size * sizeof(struct nodo *)); 
    return stack; 
} 

int isFull(struct Stack * stack) { 
    return stack->top - 1 == stack->size;
} 
  
int isEmpty(struct Stack * stack) {  
    return stack->top == -1;
} 
  
void push(struct Stack * stack, struct nodo * node) { 
    if (isFull(stack)) 
        return; 
    stack->array[++stack->top] = node; 
} 
  
struct nodo * pop(struct Stack * stack) 
{ 
    if (isEmpty(stack)) 
        return NULL; 
    return stack->array[stack->top--]; 
} 
  
struct nodo * peek(struct Stack * stack) 
{ 
    if (isEmpty(stack)) 
        return NULL; 
    return stack->array[stack->top]; 
} 