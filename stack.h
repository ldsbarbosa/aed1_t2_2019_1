#ifndef _STACK_H_
#define _STACK_H_

#include "arvore.h"

struct Stack  { 
    int size; 
    int top; 
    struct nodo **array; 
}; 

struct Stack * create(int size);

int isFull(struct Stack* stack);
  
int isEmpty(struct Stack* stack);
  
void push(struct Stack* stack, struct nodo * node);
  
struct nodo * pop(struct Stack* stack);

struct nodo * peek(struct Stack* stack);

#endif 