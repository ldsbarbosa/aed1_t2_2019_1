#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"
#include "stack.h"

#define MAX 100 


struct nodo * inicializa_arvore(int entradas, int * valores) {
    struct nodo * tree = NULL;

    if(entradas <= 0 || valores == NULL)
        return 0;
        
    int i;
    for(i = 0; i < entradas; i++)
        tree = insere_nodo(tree, valores[i]);        

    return tree;       
}

struct nodo * menor_valor(struct nodo * raiz) 
{ 
    struct nodo * current = raiz; 
  
    while (current && current->esq != NULL) 
        current = current->esq; 
  
    return current; 
} 
  

struct nodo * insere_nodo(struct nodo* raiz, int valor) {

    if(raiz == NULL) {
        struct nodo * newNodo = (struct nodo *) malloc(sizeof(struct nodo));
        newNodo->valor = valor;
        newNodo->esq = newNodo->dir = NULL;
        return newNodo;
    }
    else if(valor > raiz->valor) {
        raiz->esq = insere_nodo(raiz->esq, valor);
    }
        
    else if(valor < raiz->valor) {
        raiz->dir = insere_nodo(raiz->dir, valor);
    }
        
    return raiz;
}

struct nodo * remove_nodo(struct nodo * raiz, int valor) {
    struct nodo * getNode;
    
    if(raiz == NULL)
        return 0;
    
    getNode = busca(raiz, valor);

    if(getNode == NULL)
        return 0;
    
    if (raiz->esq == NULL) 
    { 
        struct nodo * tmp = raiz->dir; 
        free(raiz); 
        return tmp; 
    } 
    else if (raiz->dir == NULL) 
    { 
        struct nodo * tmp = raiz->esq; 
        free(raiz); 
        return tmp; 
    } 

    struct nodo * tmp = menor_valor(raiz->esq); 

    raiz->valor = tmp->valor; 
    raiz->esq = remove_nodo(raiz->esq, tmp->valor); 

    return raiz;


}

struct nodo * busca(struct nodo * raiz, int valor) {
    
    if (raiz == NULL)
        return 0;
  
    if (valor < raiz->valor) 
        raiz->esq = busca(raiz->esq, valor); 
  
    else if (valor > raiz->valor) 
        raiz->dir = busca(raiz->dir, valor);
    //founded!
    
    return raiz;
}

void imprime(int * valores, int tamanho) {
    if(tamanho > 0) 
        if(valores) {
            int i = 0;
            printf("Imprime");
            while(i < tamanho) {
                printf("%d ", valores[i]);
                if(i == 10) {
                    printf("\n");
                        i = 1;
                        tamanho -= 10;
                }

                i++;
            }
        }
}
int infix(struct nodo * raiz, int * resultado) {
    
    struct nodo * current,* ant;
    int i = 0;
    
    if(raiz == NULL)
        return 0;

    current = raiz;
    
    while(current != NULL) {
        if(current->esq == NULL) {
            resultado[i] = current->valor;
            current = current->dir;
            i++;
        } else {

            ant = current->esq;
            while(ant->dir != NULL && ant->dir != current)
                ant = ant->dir;


            if(ant->dir == NULL) {
                ant->dir = current;
                current = current->esq;
            } else {
                ant->dir = NULL;
                resultado[i] = current->valor;
                current = current->dir;
                i++;
            } 
        } 
    } 

    return *(resultado);

}

int postfix(struct nodo * raiz, int * resultado) {
    if(raiz == NULL) 
        return 0;
    int i = 0;
    struct Stack * stack = create(MAX); 

    printf("postfix\n");
    do { 

        while (raiz) { 
 
            if (raiz->esq) 
                push(stack, raiz->esq); 
                

            push(stack, raiz); 
            raiz = raiz->dir; 
        } 
  
        raiz = pop(stack); 

        if (raiz->esq && peek(stack) == raiz->esq){ 
            pop(stack);  
            push(stack, raiz); 
            raiz = raiz->esq;  
        } else {
            resultado[i] = raiz->valor;
            printf("%d - ", raiz->valor);
            i++;
            raiz = NULL; 
        } 
    } while (!isEmpty(stack));

    return *(resultado);
}

// Not working
int prefix(struct nodo * raiz, int * resultado) {
    printf("%d, %d", raiz->valor, resultado[0]);
    return 0;
}

// Not working
int abrangencia(struct nodo * raiz, int * resultado) {
    int alturaTree = altura(raiz);
    int i;

    for (i = 1; i <= alturaTree; i++) 
        nodos_por_nivel(raiz, i);

        
    return *(resultado);
}

void nodos_por_nivel(struct nodo * raiz, int nivel) {
    if (raiz == NULL) 
        return; 
    
    if (nivel == 1)
        //Get the
        printf("%d", raiz->valor);

    else if (nivel > 1) { 
        nodos_por_nivel(raiz->esq, nivel-1); 
        nodos_por_nivel(raiz->dir, nivel-1); 
    } 
}

int numero_elementos(struct nodo * raiz) {
    if(raiz == NULL){
        return 0;
    }
    else{
        return 1 + numero_elementos(raiz->esq) + numero_elementos(raiz->dir);
    }
}

int balanceada(struct nodo * raiz) { 
    int subArvoreEsq, subArvoreDir; 
  
    if(raiz == NULL) 
        return 1;  
  
    subArvoreEsq = altura(raiz->esq); 
    subArvoreDir = altura(raiz->dir); 
  
    if( abs(subArvoreEsq-subArvoreDir) <= 1 && balanceada(raiz->esq) && balanceada(raiz->dir)) 
        return 1; 
  
    return 0; 
} 

void remove_todos(struct nodo * raiz) {

    if (raiz == NULL) 
        return;

    remove_todos(raiz->esq); 
    remove_todos(raiz->dir); 
    
    free(raiz);
}

int altura(struct nodo * raiz) { 
   if (raiz == NULL)  
       return 0; 
   
    int caminhoEsq = altura(raiz->esq); 
    int caminhoDir = altura(raiz->dir); 

    if (caminhoEsq > caminhoDir)  
        return(caminhoEsq+1); 
    else return(caminhoDir+1); 

}  